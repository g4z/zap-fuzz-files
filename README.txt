
# ZAP Fuzz Files

## Install

```
# change to ZAP fuzz file directory
cd $HOME/.ZAP/fuzzers

# clone using SSH
git clone git@gitlab.com:g4z/zap-fuzz-files.git

# ...or clone using HTTP
git clone https://gitlab.com/g4z/zap-fuzz-files.git
```
